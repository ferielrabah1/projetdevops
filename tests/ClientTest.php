<?php

namespace App\Tests;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testClientEntity()
    {
        // Create an instance of Client
        $client = new Client();

        // Set some values
        $client->setCin("123456789");
        $client->setNom("Rabah");
        $client->setPrenom("Feriel");
        $client->setAdresse("Adresse 1");

        // Assertions
	$this->assertNull($client->getId());
        $this->assertEquals("123456789", $client->getCin());
        $this->assertEquals("Rabah", $client->getNom());
        $this->assertEquals("Feriel", $client->getPrenom());
        $this->assertEquals("Adresse 1", $client->getAdresse());

    }
}

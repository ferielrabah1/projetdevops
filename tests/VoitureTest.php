<?php

namespace App\Tests;

use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase
{
    public function testVoitureEntity()
    {
        // Create an instance of Voiture
        $voiture = new Voiture();

        // Set some values
        $voiture->setSerie("ABC123");
        $voiture->setDateMiseEnMarche(new \DateTime('2022-01-01'));
        $voiture->setModele("Model XYZ");
        $voiture->setPrixJour(100.50);

        // Assertions
	$this->assertNull($voiture->getId());
        $this->assertEquals("ABC123", $voiture->getSerie());
        $this->assertInstanceOf(\DateTime::class, $voiture->getDateMiseEnMarche());
        $this->assertEquals("Model XYZ", $voiture->getModele());
        $this->assertEquals(100.50, $voiture->getPrixJour());

    }
}

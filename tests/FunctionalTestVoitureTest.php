<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureFunctionalTest extends WebTestCase
{
    public function testShouldDisplayVoitureIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Voiture index');
    }

    public function testShouldDisplayCreateNewVoiture()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Voiture');
    }

    public function testShouldAddNewVoiture()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();

        $serie = 'Voiture Test Series';

        $form = $buttonCrawlerNode->form([
            'voiture[serie]'        => $serie,
            'voiture[dateMiseEnMarche][year]' => '2023',
            'voiture[dateMiseEnMarche][month]' => '1',
            'voiture[dateMiseEnMarche][day]'   => '1',
            'voiture[modele]'       => 'Test Model',
            'voiture[prixJour]'     => '100.00',
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', $serie);
    }
}
